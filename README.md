#Code Challenges
##Overview
 This is a maven project where I practice coding challenges.  Each coding challenge has its own problem statement which can be found in root/ProblemStatements directory.  Each problem statement maps to a similarly named package under com.devreed.algorithms.  Each has a test directory under src/test/java/com.devreed.algorithms.  Any code challenges which are not complete will say so on the first line of their problem statement.
 <br><br>
 Note:  Some of these examples are light on test cases in the project, but all solutions score a 100% when run against the many test cases on the sites where these problems came from.    
     
## Building the project
'code-challenges' is the base directory of this project.  Run mvn clean install from this directory to build the project and execute test cases.