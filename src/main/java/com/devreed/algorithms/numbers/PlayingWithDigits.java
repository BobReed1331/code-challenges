package com.devreed.algorithms.numbers;

import java.util.Stack;


/**
 * See Playing With Digits problem statement
 */
public class PlayingWithDigits {

    /**
     * Method required as a hook to code wars
     * @param number the number which will be modified
     * @param power the power we are applying
     * @return the required number as per the problem statement, or -1 if there is no solution
     */
    public static long digPow(int number, int power) {

        Stack<Integer> ints = new Stack<Integer>();
        splitDigits(ints, number);

        int total = createTotal(power, ints);

        if(canBeDividedEvenly(number, total)){
            return total / number;
        }

        return -1;
    }

    private static boolean canBeDividedEvenly(int number, int total) {
        return total % number == 0;
    }

    /**
     * creates the total by applying the power to the proper digit and then summing, as per the problem
     * statement
     * @param power starting power to apply to each digit
     * @param digits the stack of digits which we can pop off and apply a given power to
     * @return the total as an int
     */
    private static int createTotal(int power, Stack<Integer> digits) {
        int total = 0;
        while(! digits.isEmpty()) {
            total = total + (int)(Math.pow(digits.pop(), power));
            power++;
        }
        return total;
    }

    /**
     * Recursive function to split the numbers digits and place them in a stack,
     * since the problem statement requires working with them in the reverse order
     * of the maths related to splitting the number apart
     *
     * converting the number to a string and working that way would have bee easier, but
     * I preferred math
     * @param ints a stack of digits
     * @param numberToSplit
     */
    private static void splitDigits(Stack<Integer> ints, int numberToSplit) {

        ints.push(numberToSplit % 10);
        if(reducePower(numberToSplit) > 0) {
            splitDigits(ints, reducePower(numberToSplit));
        }
    }

    /**
     * reduces a given number by a power of ten
     * @param number
     * @return the reduced number represented as an integer
     */
    private static int reducePower(int number){
        return number / 10;
    }
}
