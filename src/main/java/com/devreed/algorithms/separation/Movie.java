package com.devreed.algorithms.separation;

import com.google.common.base.MoreObjects;

import java.util.List;
import java.util.Objects;

public class Movie {

	private String name;
	private List<Role> roles;
	
	public Movie(String name, List<Role> roles){
		this.name = name;
		this.roles = roles;
	}
	
	public String getName() {
		return name;
	}

	public List<Role> getRoles(){
		return this.roles;
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Movie movie = (Movie) o;
        return Objects.equals(name, movie.name) &&
                Objects.equals(roles, movie.roles);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, roles);
    }

    @Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("name", name)
				.add("roles", roles)
				.toString();
	}
}
