package com.devreed.algorithms.separation;

import com.google.common.base.MoreObjects;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Actor {

	private String name;
	private List<Role> roles;
	
	public Actor(String name, List<Role> roles){
		this.name = name;
		this.roles = roles;
	}

	public Actor(List<Role> roles){
		this.roles = roles;
	}

	public Actor(){
		this.roles = new ArrayList<Role>();
	}
	
	public List<Role> getRoles(){
		return roles;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Actor actor = (Actor) o;
		return Objects.equals(name, actor.name) &&
				Objects.equals(roles, actor.roles);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, roles);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("name", name)
				.add("roles", roles)
				.toString();
	}
}
