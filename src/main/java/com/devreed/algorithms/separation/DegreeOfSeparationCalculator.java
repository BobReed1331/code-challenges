package com.devreed.algorithms.separation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DegreeOfSeparationCalculator {
	
	Map<Actor, Object> visitedActors = null;
	Actor actor1 = null;
	Actor actor2 = null;
	
	public DegreeOfSeparationCalculator(){}

	/**
	 * Calculates the degrees of separation between one actor and the next
	 * @param a1 Actor 1
	 * @param a2 Actor 2
	 * @return the degrees of separation as an int
	 */
	public int calculateDegreesOfSeparation(Actor a1, Actor a2){
		this.visitedActors = new HashMap<>();
		this.actor1 = a1;
		this.actor2 = a2;
		
		int degree = 0;
		if( compareActors(actor1, actor2) ){
			return degree;
		}
		
		markActorAsVisited(a1);
		List<Actor> actors = getAdjacentUnvisitedActors(actor1);
		return searchActors(actors, ++degree);	
	}

    /**
     * Searches actors and tracks degrees separation until a match is found
     * @param actorList the list of actors to compare against
     * @param degree current degree of separation
     * @return a number representing the degrees of separation
     */
	private int searchActors(List<Actor> actorList, int degree){
		List<Actor> adjacentActors = new ArrayList<Actor>();
		
		if(actorList.isEmpty()){
			return -1;
		}
		
		for(Actor actor : actorList){
			if( !actorVisited(actor) ){
				markActorAsVisited(actor);
				if( compareActors(this.actor2, actor) ){
					return degree;
				}
				
				adjacentActors.addAll(getAdjacentUnvisitedActors(actor));
			}
		}
		
		return searchActors(adjacentActors, ++degree);
	}

    /**
     * Determines whether an actor has already been visited
     * @param a1 the actor
     * @return true if it has been visited, otherwise false
     */
	private boolean actorVisited(Actor a1) {
		if( visitedActors.containsKey(a1) ){
			return true;
		}
		return false;
	}

    /**
     * Marks an actor as visited by adding it to a map of visited actors
     * @param a1 the actor to mark as visited
     */
	private void markActorAsVisited(Actor a1) {
		this.visitedActors.put(a1, null);
	}

    /**
     * Finds all actors in movies that the current actor has been a part of
     * @param actor find actors adjacent to this one
     * @return returns the list of all actors adjacent to the initial actor
     */
	private List<Actor> getAdjacentUnvisitedActors(Actor actor){
		List<Actor> actors = new ArrayList<Actor>();

		List<Role> actorRoles = actor.getRoles();
		for(Role role : actorRoles){
			Movie movie = role.getMovie();
			List<Role> movieRoles = movie.getRoles();
			movieRoles.stream().forEach( mr -> {
				Actor a = mr.getActor();
				if(! actorVisited(a)){
					actors.add(a);
				}
			});
		}

		return actors;
	}

    /**
     * Compares the actors to determine if they are the same person
     * @param a1 actor 1
     * @param a2 actor 2
     * @return true if they are the same, false otherwise
     */
	private boolean compareActors(Actor a1, Actor a2){
		if(a1.equals(a2)){
			return true;
		}
		return false;
	}
}
