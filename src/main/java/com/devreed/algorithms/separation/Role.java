package com.devreed.algorithms.separation;

import com.google.common.base.MoreObjects;

public class Role {

	private Movie movie;
	private Actor actor;
	private String roleName;
	
	public Role(Movie movie, Actor actor, String roleName){
		this.movie = movie;
		this.actor = actor;
		this.roleName = roleName;
	}
	
	public Movie getMovie(){
		return this.movie;
	}
	
	public Actor getActor(){
		return this.actor;
	}

    public String getRoleName() {
        return roleName;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("movie", movie)
                .add("actor", actor)
                .add("roleName", roleName)
                .toString();
    }
}
