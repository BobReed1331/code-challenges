///**
// * This class is only here to combine all of the classes within ContactsApp into one submission for
// * hackerRank
// */


//package com.devreed.algorithms.contactsApp;
//
//import java.util.HashMap;
//import java.util.LinkedList;
//import java.util.Map;
//import java.util.Optional;
//import java.util.Queue;
//import java.util.Scanner;
//
//public class Solution {
//
//    static enum OpType {
//        add,
//        find
//    }
//
//    static class ContactNode {
//
//        private Character value;
//        private Map<Character, ContactNode> availableNodes = new HashMap();
//        private ContactCounter numContactsFromThisNode = new ContactCounter();
//        private boolean endOfWord = false;
//
//        public ContactNode(Character value, boolean isEow) {
//            this.value = value;
//            this.endOfWord = isEow;
//            this.numContactsFromThisNode.increment();
//        }
//
//        /**
//         * @return the character value contained within this node
//         */
//        public Character getValue() {
//            return value;
//        }
//
//        /**
//         * returns a map containing all nodes that can be reached from this one
//         */
//        public Map<Character, ContactNode> getAvailableNodes() {
//            return availableNodes;
//        }
//
//        /**
//         * increments the counter indicating how many contact names
//         * are reachable from this node
//         */
//        public void incrementNumContactsBeyondThisNode() {
//            numContactsFromThisNode.increment();
//        }
//
//        /**
//         * Gets the number of contacts that can be found from this node
//         *
//         * @return a count of contact names beyond this node
//         */
//        public int getNumContactsFromThisNode() {
//            return numContactsFromThisNode.getCount();
//        }
//
//        /**
//         * a method indicating whether this node should be treated as the last
//         * character of a Contact Name
//         *
//         * @return true if this node is the last in a sequence of nodes that makes up a valid contact
//         * name
//         */
//        public boolean isEndOfName() {
//            return endOfWord;
//        }
//
//        @Override
//        public boolean equals(Object o) {
//            if (this == o) return true;
//            if (o == null || getClass() != o.getClass()) return false;
//
//            ContactNode that = (ContactNode) o;
//
//            if (endOfWord != that.endOfWord) return false;
//            if (!value.equals(that.value)) return false;
//            if (!availableNodes.equals(that.availableNodes)) return false;
//            return numContactsFromThisNode.equals(that.numContactsFromThisNode);
//        }
//
//        @Override
//        public int hashCode() {
//            int result = value.hashCode();
//            result = 31 * result + availableNodes.hashCode();
//            result = 31 * result + numContactsFromThisNode.hashCode();
//            result = 31 * result + (endOfWord ? 1 : 0);
//            return result;
//        }
//
//        @Override
//        public String toString() {
//            final StringBuilder sb = new StringBuilder("ContactNode{");
//            sb.append("value=").append(value);
//            sb.append(", availableNodes=").append(availableNodes);
//            sb.append(", numContactsFromThisNode=").append(numContactsFromThisNode);
//            sb.append(", endOfWord=").append(endOfWord);
//            sb.append('}');
//            return sb.toString();
//        }
//    }
//
//    static class ContactCounter {
//
//        private int count = 0;
//
//        public ContactCounter() {}
//
//        public void increment(){
//            count++;
//        }
//
//        public int getCount() {
//            return count;
//        }
//
//        @Override
//        public boolean equals(Object o) {
//            if (this == o) return true;
//            if (o == null || getClass() != o.getClass()) return false;
//
//            ContactCounter that = (ContactCounter) o;
//
//            return count == that.count;
//        }
//
//        @Override
//        public int hashCode() {
//            return count;
//        }
//
//        @Override
//        public String toString() {
//            final StringBuilder sb = new StringBuilder("ContactCounter{");
//            sb.append("count=").append(count);
//            sb.append('}');
//            return sb.toString();
//        }
//    }
//
//    static public interface ContactsService {
//
//        void add(String name);
//
//        int findNumberOfNamesFromPartial(String charSeq);
//
//        Map<Character, ContactNode> getContactMap();
//    }
//
//    static class ContactServiceImpl implements ContactsService {
//
//        Map<Character, ContactNode> contactMap = new HashMap<>();
//
//        /**
//         * Takes a name and turns it into a sequence of character nodes
//         *
//         * @param name A string, also known as a 'partial', which represents the partial name of a
//         *             contact
//         */
//        @Override
//        public void add(String name) {
//            if (name != null && name.length() > 0) {
//                String sanitizedInput = sanitizeInput(name);
//                addNodesFromString(sanitizedInput);
//            }
//        }
//
//        /**
//         * Determines the number of names in the address book which can be found based on the initial
//         * 'partial'
//         *
//         * @param nameToFind A string, also known as a 'partial', which represents the partial name of a
//         *                   contact
//         * @return the number of names
//         */
//        @Override
//        public int findNumberOfNamesFromPartial(String nameToFind) {
//            String sanitizedInput = sanitizeInput(nameToFind);
//            // turn charSeq into a list of characters
//            Queue<Character> nameAsCharQueue = createCharQueue(sanitizedInput);
//            ContactCounter currentCount = new ContactCounter();
//
//            //is there a root node for the first character?
//            // navigate to last node
//            Optional<ContactNode> rootNodeForSearchOpt = findLastQueuedNode(contactMap, nameAsCharQueue);
//
//            if (!rootNodeForSearchOpt.isPresent()) {
//                return currentCount.getCount();
//            }
//
//            return rootNodeForSearchOpt.get().getNumContactsFromThisNode();
//        }
//
//
//        //TODO refactor this out and use existing method or create a new method that allows getting a
//        // node at the location of the partial.
//
//        /**
//         * This is largely here for ease of testing and I would not recommend doing this in production
//         * code
//         *
//         * @return the map containing all base nodes
//         */
//        @Override
//        public Map<Character, ContactNode> getContactMap() {
//            return contactMap;
//        }
//
//
//        private String sanitizeInput(String name) {
//            return name.toLowerCase();
//        }
//
//        private void addNodesFromString(String name) {
//            int count = 1;
//            char[] characters = name.toCharArray();
//            Map<Character, ContactNode> currentMap = contactMap;
//
//            for (Character c : characters) {
//                ContactNode currentNode = currentMap.get(c);
//
//                if (nodeExistsInMap(currentNode, currentMap)) {
//                    //node exists
//                    currentNode.incrementNumContactsBeyondThisNode();
//                    currentMap = currentNode.getAvailableNodes();
//                } else {
//                    // create a new node and add it to the map
//                    currentNode = createNewNode(c, count, name.length());
//                    addNewNodeToMap(currentNode, currentMap);
//                    currentMap = getNodeMapFromNode(currentNode);
//                }
//
//                count++;
//            }
//        }
//
//        private ContactNode createNewNode(char c, int charInNameCount, int nameLength) {
//            boolean isEndOfName = isLastChar(charInNameCount, nameLength);
//            return new ContactNode(c, isEndOfName);
//        }
//
//        private Map<Character, ContactNode> getNodeMapFromNode(ContactNode currentNode) {
//            return currentNode.getAvailableNodes();
//        }
//
//        private void addNewNodeToMap(ContactNode node, Map<Character, ContactNode> contactMap) {
//            contactMap.put(node.getValue(), node);
//        }
//
//        private boolean nodeExistsInMap(ContactNode contactNode, Map<Character, ContactNode> currentMap) {
//            if (contactNode == null) {
//                return false;
//            }
//            return true;
//        }
//
//        private boolean isLastChar(int count, int length) {
//            return count == length ? true : false;
//        }
//
//        private Queue<Character> createCharQueue(String str) {
//            Queue<Character> charQueue = new LinkedList<>();
//
//            for (int i = 0; i < str.length(); i++) {
//                charQueue.add(str.charAt(i));
//            }
//
//            return charQueue;
//        }
//
//        private Optional<ContactNode> findLastQueuedNode(Map<Character, ContactNode> contactMap, Queue<Character> charQueue) {
//            //get first char
//            Character currentChar = charQueue.poll();
//            ContactNode currNode = contactMap.get(currentChar);
//
//            if (currNode == null) {
//                return Optional.empty();
//            }
//
//            if (currNode != null && charQueue.isEmpty()) {
//                return Optional.of(currNode);
//            }
//
//            return findLastQueuedNode(currNode.getAvailableNodes(), charQueue);
//        }
//
//        //TODO create a findNames method which returns the actual names found from the partial
////    public int findNumberOfNamesFromPartial(String nameToFind) {
////        // turn charSeq into a list of characters
////        Queue<Character> nameAsCharQueue = createCharQueue(nameToFind);
////        ContactCounter currentCount = new ContactCounter();
////
////        //is there a root node for the first character?
////        // navigate to last node
////        Optional<ContactNode> rootNodeForSearchOpt = findLastQueuedNode(contactMap, nameAsCharQueue);
////
////        if(! rootNodeForSearchOpt.isPresent()){
////            return currentCount.getCount();
////        }
////
////        ContactNode node = rootNodeForSearchOpt.get();
////        if(isValidName(node)){
////            currentCount.increment();
////        }
////
////        // recursive search to looking for 'end of word' property on each node
////        findNumberOfContacts(node.getAvailableNodes(), currentCount);
////
////        return currentCount.getCount();
////    }
////
////    private boolean isValidName(ContactNode node) {
////        return node.isEndOfName();
////    }
////
////    private void findNumberOfContacts(Map<Character, ContactNode> contactNode, ContactCounter currentCount) {
////
////        for( ContactNode node : contactNode.values()){
////            if(node.isEndOfName()){
////                currentCount.increment();
////            }
////
////            findNumberOfContacts(node.getAvailableNodes(), currentCount);
////        }
////    }
//    }
//
//
//    /**
//     * HackerRank requires this hook for submitting test code
//     * @param args not used
//     */
//    public static void main(String[] args) {
//        ContactsService contactService = new ContactServiceImpl();
//
//        Scanner in = new Scanner(System.in);
//        int n = in.nextInt();
//
//        for (int a0 = 0; a0 < n; a0++) {
//            String op = in.next();
//            String contact = in.next();
//
//            //TODO catch exceptions for valueOf
//            if (OpType.valueOf(op) == OpType.add) {
//                contactService.add(contact);
//            } else if (OpType.valueOf(op) == OpType.find) {
//                System.out.println(contactService.findNumberOfNamesFromPartial(contact));
//            }
//        }
//    }
//}
