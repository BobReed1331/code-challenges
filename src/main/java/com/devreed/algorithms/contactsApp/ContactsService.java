package com.devreed.algorithms.contactsApp;


import java.util.Map;

public interface ContactsService {

    void add(String name);

    int findNumberOfNamesFromPartial(String charSeq);

    Map<Character, ContactNode> getContactMap();
}
