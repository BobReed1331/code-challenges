package com.devreed.algorithms.contactsApp;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Optional;
import java.util.Queue;

public class ContactServiceImpl implements ContactsService {

    Map<Character, ContactNode> contactMap = new HashMap<>();

    /**
     * Takes a name and turns it into a sequence of character nodes
     *
     * @param name A string, also known as a 'partial', which represents the partial name of a
     *             contact
     */
    @Override
    public void add(String name) {
        if (name != null && name.length() > 0) {
            String sanitizedInput = sanitizeInput(name);
            addNodesFromString(sanitizedInput);
        }
    }

    /**
     * Determines the number of names in the address book which can be found based on the initial
     * 'partial'
     *
     * @param nameToFind A string, also known as a 'partial', which represents the partial name of a
     *                   contact
     * @return the number of names
     */
    @Override
    public int findNumberOfNamesFromPartial(String nameToFind) {
        String sanitizedInput = sanitizeInput(nameToFind);

        // turn charSeq into a list of characters
        Queue<Character> nameAsCharQueue = createCharQueue(sanitizedInput);
        ContactCounter currentCount = new ContactCounter();

        //is there a root node for the first character?
        // navigate to last node
        Optional<ContactNode> rootNodeForSearchOpt = findLastQueuedNode(contactMap, nameAsCharQueue);

        if (!rootNodeForSearchOpt.isPresent()) {
            return currentCount.getCount();
        }

        return rootNodeForSearchOpt.get().getNumContactsFromThisNode();
    }


    //TODO refactor this out and use existing method or create a new method that allows getting a
    // node at the location of the partial, rather than returning the mutable map

    /**
     * This is largely here for ease of testing and I would not recommend doing this in production
     * code
     *
     * @return the map containing all base nodes
     */
    @Override
    public Map<Character, ContactNode> getContactMap() {
        return contactMap;
    }


    /**
     * Sanitizes the input so it is in a parseable format
     * @param name the name as a string
     * @return lowercase representation of the string
     */
    private String sanitizeInput(String name) {
        return name.toLowerCase();
    }

    /**
     * Takes a string and creates a map of traversable individual character Nodes
     * @param name a name as a string
     */
    private void addNodesFromString(String name) {
        int count = 1;
        char[] characters = name.toCharArray();
        Map<Character, ContactNode> currentMap = contactMap;

        for (Character c : characters) {
            ContactNode currentNode = currentMap.get(c);

            if (nodeExists(currentNode)) {
                //node exists
                currentNode.incrementNumContactsBeyondThisNode();
                currentMap = currentNode.getAvailableNodes();
            } else {
                // create a new node and add it to the map
                currentNode = createNewNode(c, count, name.length());
                addNewNodeToMap(currentNode, currentMap);
                currentMap = getNodeMapFromNode(currentNode);
            }

            count++;
        }
    }

    /**
     * Creates a new character node which represents one character of a name
     * @param c the character
     * @param charInNameCount the position of the character in the name
     * @param nameLength the total length of the name
     * @return the new ContactNode
     */
    private ContactNode createNewNode(char c, int charInNameCount, int nameLength) {
        boolean isEndOfName = isLastChar(charInNameCount, nameLength);
        return new ContactNode(c, isEndOfName);
    }

    /**
     * Gets the Contact Node's map of nodes which are accessible from it
     * @param currentNode the node which contains the map of other nodes
     * @return A map of all reachable character nodes from this contact node
     */
    private Map<Character, ContactNode> getNodeMapFromNode(ContactNode currentNode) {
        return currentNode.getAvailableNodes();
    }

    /**
     * Adds the new contact node to a given map of contact nodes
     * @param node
     * @param contactMap
     */
    private void addNewNodeToMap(ContactNode node, Map<Character, ContactNode> contactMap) {
        contactMap.put(node.getValue(), node);
    }

    /**
     * Determines whether or not a node exists
     * @param contactNode the node
     * @return true if node exists, else false
     */
    private boolean nodeExists(ContactNode contactNode) {
        if (contactNode == null) {
            return false;
        }
        return true;
    }

    /**
     * Determines if the character is the last one in a name
     * @param characterLocationInName the position of the character in the name
     * @param nameLength the total length of the name
     * @return true if the character is the last one in the name
     */
    private boolean isLastChar(int characterLocationInName, int nameLength) {
        return characterLocationInName == nameLength ? true : false;
    }

    /**
     * Creates a Queue of characters from a given string
     * @param str the input string
     * @return a queue of chars representing the string
     */
    private Queue<Character> createCharQueue(String str) {
        Queue<Character> charQueue = new LinkedList<>();

        for (int i = 0; i < str.length(); i++) {
            charQueue.add(str.charAt(i));
        }

        return charQueue;
    }

    /**
     * Navigates through the contact map by traversing the charQueue, and returns what is found at the
     * last character in the Queue.
     * @param contactMap a map of contact nodes
     * @param charQueue the queue used to navigate the contact map
     * @return A contact node if it exists, empty if it does not.  Null if the map returns null
     */
    private Optional<ContactNode> findLastQueuedNode(Map<Character, ContactNode> contactMap, Queue<Character> charQueue) {
        //get first char
        Character currentChar = charQueue.poll();
        ContactNode currNode = contactMap.get(currentChar);

        if (currNode == null) {
            return Optional.empty();
        }

        if (currNode != null && charQueue.isEmpty()) {
            return Optional.of(currNode);
        }

        return findLastQueuedNode(currNode.getAvailableNodes(), charQueue);
    }
}
