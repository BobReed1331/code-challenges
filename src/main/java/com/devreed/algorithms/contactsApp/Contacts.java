package com.devreed.algorithms.contactsApp;

import java.util.Scanner;

import static com.devreed.algorithms.contactsApp.OpType.add;
import static com.devreed.algorithms.contactsApp.OpType.find;

public class Contacts {

    private static ContactServiceImpl contactService = new ContactServiceImpl();

    /*
        There is no test hook for this solution.  The service can be called directly from test cases
     */

    /**
     * HackerRank requires this hook for submitting test code
     *
     * @param args not used
     */
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();

        for (int a0 = 0; a0 < n; a0++) {
            String op = in.next();
            String contact = in.next();

            //TODO catch exceptions for valueOf
            if (OpType.valueOf(op) == add) {
                contactService.add(contact);
            } else if (OpType.valueOf(op) == find) {
                System.out.println(contactService.findNumberOfNamesFromPartial(contact));
            }
        }
    }
}
