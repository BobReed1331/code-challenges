package com.devreed.algorithms.contactsApp;


/**
 * A class which controls the incrementing of a counter variable
 */
public class ContactCounter {

    private int count = 0;

    public ContactCounter() {
    }

    /**
     * Increments the counter by 1
     */
    public void increment() {
        count++;
    }

    public int getCount() {
        return count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ContactCounter that = (ContactCounter) o;

        return count == that.count;
    }

    @Override
    public int hashCode() {
        return count;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ContactCounter{");
        sb.append("count=").append(count);
        sb.append('}');
        return sb.toString();
    }
}
