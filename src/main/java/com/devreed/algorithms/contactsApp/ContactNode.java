package com.devreed.algorithms.contactsApp;


import java.util.HashMap;
import java.util.Map;

/**
 * A node representing one character of a contact name
 */
public class ContactNode {

    private Character value;
    private Map<Character, ContactNode> availableNodes = new HashMap();
    private ContactCounter numContactsFromThisNode = new ContactCounter();
    private boolean endOfWord = false;

    public ContactNode(Character value, boolean isEow) {
        this.value = value;
        this.endOfWord = isEow;
        this.numContactsFromThisNode.increment();
    }

    /**
     * @return the character value contained within this node
     */
    public Character getValue() {
        return value;
    }

    /**
     * returns a map containing all nodes that can be reached from this one
     */
    public Map<Character, ContactNode> getAvailableNodes() {
        return availableNodes;
    }

    /**
     * increments the counter indicating how many contact names
     * are reachable from this node
     */
    public void incrementNumContactsBeyondThisNode() {
        numContactsFromThisNode.increment();
    }

    /**
     * Gets the number of contacts that can be found from this node
     *
     * @return a count of contact names beyond this node
     */
    public int getNumContactsFromThisNode() {
        return numContactsFromThisNode.getCount();
    }

    /**
     * a method indicating whether this node should be treated as the last
     * character of a Contact Name
     *
     * @return true if this node is the last in a sequence of nodes that makes up a valid contact
     * name
     */
    public boolean isEndOfName() {
        return endOfWord;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ContactNode that = (ContactNode) o;

        if (endOfWord != that.endOfWord) return false;
        if (!value.equals(that.value)) return false;
        if (!availableNodes.equals(that.availableNodes)) return false;
        return numContactsFromThisNode.equals(that.numContactsFromThisNode);
    }

    @Override
    public int hashCode() {
        int result = value.hashCode();
        result = 31 * result + availableNodes.hashCode();
        result = 31 * result + numContactsFromThisNode.hashCode();
        result = 31 * result + (endOfWord ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ContactNode{");
        sb.append("value=").append(value);
        sb.append(", availableNodes=").append(availableNodes);
        sb.append(", numContactsFromThisNode=").append(numContactsFromThisNode);
        sb.append(", endOfWord=").append(endOfWord);
        sb.append('}');
        return sb.toString();
    }
}
