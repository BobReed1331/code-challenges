package com.devreed.algorithms.binarySearch;

/**
 * HackerRank has bound me to this awful Node class.
 * Give me immutable objects with getters please!
 */
public class Node {
    int data;
    Node left;
    Node right;
}
