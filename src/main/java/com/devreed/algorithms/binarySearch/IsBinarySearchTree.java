package com.devreed.algorithms.binarySearch;

import java.util.ArrayList;
import java.util.List;

public class IsBinarySearchTree {

    /**
     *  Checks to see whether or not a given root node is the roof of the BST
     * @param root the root of the BST
     * @return true if it is a BST, false if not
     */
    boolean checkBST(Node root) {
        List<Integer> treeAsOrderedList = new ArrayList<>();

        if (root == null){
            return false;
        }

        traverse(treeAsOrderedList, root.left);
        treeAsOrderedList.add(root.data);
        traverse(treeAsOrderedList, root.right);

        boolean isBinarySearchTree = validateOrdering(treeAsOrderedList);
        return isBinarySearchTree;
    }

    /**
     * asserts that the ordering is the proper order for a valid BST
     * @param treeAsOrderedList  the BST in the form of an ordered list
     * @return
     */
    private boolean validateOrdering(List<Integer> treeAsOrderedList) {
        boolean isBinarySearchTree = true;

        for(int i=1; i < treeAsOrderedList.size(); i++){
            if(treeAsOrderedList.get(i) <= treeAsOrderedList.get(i-1)){
                isBinarySearchTree = false;
            }
        }
        return isBinarySearchTree;
    }

    /**
     * Recursive function which builds a list of nodes.  Nodes are added
     * under by traversing all the way left, then the node itself, and then the
     * right traversal
     * @param treeAsOrderedList the list that is being built up during recursion
     * @param node the current node
     */
    private void traverse(List<Integer> treeAsOrderedList, Node node) {

        if(node != null){
            traverse(treeAsOrderedList, node.left);
            treeAsOrderedList.add(node.data);
            traverse(treeAsOrderedList, node.right);
        }
    }
}

