package com.devreed.algorithms.arrays;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * See LeftShiftArray problem statement
 */
public class LeftShiftArray {

    /**
     * HackerRank requires this hook for submitting test code
     * @param args not used
     */
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int k = in.nextInt();
        Integer a[] = new Integer[n];
        for(int a_i=0; a_i < n; a_i++){
            a[a_i] = in.nextInt();
        }

        List<Integer> result = shiftLeft(a, k);
        printResults(result);
    }

    /**
     * Printing results is required as part of the solution
     * @param result
     */
    private static void printResults(List<Integer> result) {
        for(Integer i : result){
            System.out.print(i + " ");
        }
    }

    /**
     * Used for test cases
     * @return the list of shifted integers
     * @throws FileNotFoundException if the file cannot be found on the filesystem
     */
    public static List<Integer> test() throws FileNotFoundException {
        Scanner in = new Scanner(new File("src/main/resources/arrayShift/input.txt"));
        int n = in.nextInt();
        int k = in.nextInt();
        Integer[] arr = createArray(in, n);

        List<Integer> result = shiftLeft(arr, k);
        printResults(result);
        return result;
    }

    /**
     * Create an array of size n, the read the values from the scanner
     * @param in
     * @param n
     * @return
     */
    public static Integer[] createArray(Scanner in, int n) {
        Integer a[] = new Integer[n];

        for(int a_i=0; a_i < n; a_i++){
            a[a_i] = in.nextInt();
        }

        return a;
    }

    /**
     *  Core part of the algorithm which is responsible for shifting the numbers before the shift
     *  position to the end of the array
     * @param integerArray  The array we are starting with prior to shifting
     * @param shiftPos  The position in the array where the shifting should happen
     * @return a completely new list that is built in the shifted format
     */
    public static List<Integer> shiftLeft(Integer[] integerArray, Integer shiftPos) {
        List<Integer> shiftedList = new ArrayList<Integer>();

        addUnshiftedValues(integerArray, shiftPos, shiftedList);
        addShiftedValues(integerArray, shiftPos, shiftedList);

        return shiftedList;
    }

    /**
     * Adds the shifted values to the end of the list
     * @param integerArray the initial array
     * @param shiftPos The position in the array where the shift will happen
     * @param shiftedList the list currently being built with shifted values
     */
    private static void addShiftedValues(Integer[] integerArray, Integer shiftPos, List<Integer> shiftedList) {
        for(int i=0; i < shiftPos; i++){
            shiftedList.add(integerArray[i]);
        }
    }

    /**
     * adds the unshifted values as the beginning of the new list
     * @param integerArray the initial array
     * @param shiftPos shiftPos The position in the array where the shift will happen
     * @param shiftedList the list currently being built
     */
    private static void addUnshiftedValues(Integer[] integerArray, Integer shiftPos, List<Integer> shiftedList) {
        for(int i = shiftPos; i < integerArray.length; i++){
            shiftedList.add(integerArray[i]);
        }
    }
}
