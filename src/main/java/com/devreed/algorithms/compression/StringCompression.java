package com.devreed.algorithms.compression;


public class StringCompression {

    /**
     *  Tests for special conditions before passing the string to the core
     *  compression algorithm
     * @param input The input string
     * @return a compressed string where repetitive values
     * are replaced with a number representing how many times
     * each repreats
     */
    public static String compressString(String input){

        if(input == null){
            return null;
        }

        if(input.isEmpty()){
            return "";
        }

        return doCompression(input).toString();
    }

    /**
     * The core of the compression algorithm
     * @param input the original string
     * @return a compressed string
     */
    private static String doCompression(String input) {
        int dupCharCounter = 1;
        char currChar = input.charAt(0);
        StringBuilder result = new StringBuilder();

        for(int i=1; i<input.length(); i++){
            char nextChar = input.charAt(i);

            if(canBeCompressed(currChar, nextChar)){
                dupCharCounter++;
            } else {
                result = appendCompressedString(result, currChar, dupCharCounter);
                currChar = nextChar;
                dupCharCounter = 1;
            }
        }

        return appendCompressedString(result, currChar, dupCharCounter).toString();
    }

    /**
     * Determines whether or not the next character matches the current character,
     * which indicates this part of the string can be further compressed
     * @param currChar the current character
     * @param nextChar the next character
     * @return true if the characters are the same
     */
    private static boolean canBeCompressed(char currChar, char nextChar) {
        return nextChar == currChar;
    }

    /**
     * Tests whether or not the character counter has been incremented past one.  This
     * would indicated the string has been compressed.
     * @param dupCharCounter the counter determining the number of repeating characters
     * @return true if the counter has be incremented past 1
     */
    private static boolean isCompressed(int dupCharCounter) {
        return dupCharCounter > 1;
    }

    /**
     * Treats the current character as the last character in the string
     * @param result
     * @param currChar
     * @param dupCharCounter
     * @return
     */
    private static StringBuilder appendCompressedString(StringBuilder result, char currChar, int dupCharCounter) {
        result.append(currChar);
        if(isCompressed(dupCharCounter)){
            result.append(dupCharCounter);
        }
        return result;
    }
}
