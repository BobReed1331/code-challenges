package com.devreed.algorithms.contactsApp;


import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ContactsTest {

    private static ContactsService contactService = new ContactServiceImpl();

    @Test
    public void testAddNames() {
        contactService.add("Bob");

        Map<Character, ContactNode> contactNodeMap = contactService.getContactMap();
        assertTrue(contactNodeMap.size() == 1);

        assertTrue(contactNodeMap.get('b').getAvailableNodes().size() == 1);
        assertTrue(contactNodeMap.get('b').getAvailableNodes().get('o').getAvailableNodes().size() == 1);
        assertTrue(contactNodeMap.get('b').getAvailableNodes().get('o').getAvailableNodes().get('b').getAvailableNodes().size() == 0);
        assertTrue(contactNodeMap.get('b').getAvailableNodes().get('o').getAvailableNodes().get('b').isEndOfName());
    }

    @Test
    public void testFindNames() {
        contactService.add("hack");
        contactService.add("hackerrank");

        Map<String, Integer> nameAndResult = new HashMap<>();
        nameAndResult.put("hac", 2);
        nameAndResult.put("hak", 0);

        nameAndResult.forEach((contactName, expectedResult) -> {
            Integer res = contactService.findNumberOfNamesFromPartial(contactName); //allowing java to autobox since service returns an int and the map holds Integers
            System.out.println(res);
            assertEquals(expectedResult, res);
        });
    }

    @Test
    public void testLetterS() {
        contactService.add("s");
        contactService.add("ss");
        contactService.add("sss");
        contactService.add("ssss");
        contactService.add("sssss");

        Map<String, Integer> nameAndResult = new HashMap<>();
        nameAndResult.put("s", 5);
        nameAndResult.put("ss", 4);
        nameAndResult.put("sss", 3);
        nameAndResult.put("ssss", 2);
        nameAndResult.put("sssss", 1);
        nameAndResult.put("ssssss", 0);

        nameAndResult.forEach((contactName, expectedResult) -> {
            Integer res = contactService.findNumberOfNamesFromPartial(contactName); //allowing java to autobox since service returns an int and the map holds Integers
            System.out.println(res);
            assertEquals(expectedResult, res);
        });
    }
}