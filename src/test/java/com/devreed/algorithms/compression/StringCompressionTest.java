package com.devreed.algorithms.compression;

import org.junit.Test;

import static org.junit.Assert.*;

public class StringCompressionTest {

    @Test
    public void testCompressStringWithNewCharAtEnd(){
        String res = StringCompression.compressString("aaabbc");

        assertEquals("a3b2c", res );
    }

    @Test
    public void testCompressStringWithDupCharAtEnd(){
        String res = StringCompression.compressString("aaaabbbcc");

        assertEquals("a4b3c2", res);
    }

    @Test
    public void testNoDupes(){
        String res = StringCompression.compressString("abcd");

        assertEquals("abcd", res);
    }
}
