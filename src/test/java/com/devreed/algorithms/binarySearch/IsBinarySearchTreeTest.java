package com.devreed.algorithms.binarySearch;

import org.junit.Test;

import static org.junit.Assert.*;


public class IsBinarySearchTreeTest {

    private static IsBinarySearchTree isBinarySearchTree = new IsBinarySearchTree();

    @Test
    public void testIsBst(){
        Node bst = createSimpleTree();
        assertTrue(isBinarySearchTree.checkBST(bst));
    }

    @Test
    public void testIsNotBst(){
        Node notBst = createIncorrectTree();
        assertFalse(isBinarySearchTree.checkBST(notBst));
    }

    @Test
    public void testRootNode(){

        Node bst = new Node();
        bst.data = 4;
        bst.left = null;
        bst.right = null;

        assertTrue(isBinarySearchTree.checkBST(bst));

    }

    @Test
    public void testDuplicateValue(){
        Node bst = createSimpleTree();
        addDuplicateValue(bst);

        assertFalse(isBinarySearchTree.checkBST(bst));
    }

    @Test
    public void testIncorrectRightSequence(){
        Node bst = createSimpleTree();

        Node incorrectNode = new Node();
        incorrectNode.data = 0;
        incorrectNode.left = null;
        incorrectNode.right = null;

        bst.right.right.right = incorrectNode;

        assertFalse(isBinarySearchTree.checkBST(bst));
    }

    @Test
    public void testIncorrectLeftSequence(){
        Node bst = createSimpleTree();

        Node incorrectNode = new Node();
        incorrectNode.data = 0;
        incorrectNode.left = null;
        incorrectNode.right = null;

        bst.right.right.right = incorrectNode;

        assertFalse(isBinarySearchTree.checkBST(bst));
    }

    @Test
    public void testIncorrectLeftRightLeftSequence(){
        Node bst = createSimpleTree();

        Node incorrectNode = new Node();
        incorrectNode.data = 0;
        incorrectNode.left = null;
        incorrectNode.right = null;

        bst.left.right.left = incorrectNode;

        assertFalse(isBinarySearchTree.checkBST(bst));
    }

    private void addDuplicateValue(Node bst) {
        Node duplicateNode = new Node();
        duplicateNode.data = 4;
        duplicateNode.left = null;
        duplicateNode.right = null;

        bst.left.right.right = duplicateNode;
    }

    /** Tree looks like
     *       4
     *    2     6
     *  1  3   5  7
     * @return
     */
    private Node createSimpleTree() {
        /* left side of tree */
        Node leftLeafOf2 = new Node();
        leftLeafOf2.data = 1;
        leftLeafOf2.left = null;
        leftLeafOf2.right = null;

        Node rightLeafOf2 = new Node();
        rightLeafOf2.data = 3;
        rightLeafOf2.left = null;
        rightLeafOf2.right = null;

        Node leftNode = new Node();
        leftNode.data = 2;
        leftNode.left = leftLeafOf2;
        leftNode.right = rightLeafOf2;

        /* right side of tree */
        Node leftLeafOf6 = new Node();
        leftLeafOf6.data = 5;
        leftLeafOf6.left = null;
        leftLeafOf6.right = null;

        Node rightLeafOf6 = new Node();
        rightLeafOf6.data = 7;
        rightLeafOf6.left = null;
        rightLeafOf6.right = null;

        Node rightNode = new Node();
        rightNode.data = 6;
        rightNode.left = leftLeafOf6;
        rightNode.right = rightLeafOf6;

        Node rootNode = new Node();
        rootNode.data = 4;
        rootNode.left = leftNode;
        rootNode.right = rightNode;

        return rootNode;
    }

    private Node createIncorrectTree() {
                /* left side of tree */
        Node leftLeafOf2 = new Node();
        leftLeafOf2.data = 3;
        leftLeafOf2.left = null;
        leftLeafOf2.right = null;

        Node rightLeafOf2 = new Node();
        rightLeafOf2.data = 3;
        rightLeafOf2.left = null;
        rightLeafOf2.right = null;

        Node leftNode = new Node();
        leftNode.data = 2;
        leftNode.left = leftLeafOf2;
        leftNode.right = rightLeafOf2;

        /* right side of tree */
        Node leftLeafOf6 = new Node();
        leftLeafOf6.data = 5;
        leftLeafOf6.left = null;
        leftLeafOf6.right = null;

        Node rightLeafOf6 = new Node();
        rightLeafOf6.data = 7;
        rightLeafOf6.left = null;
        rightLeafOf6.right = null;

        Node rightNode = new Node();
        rightNode.data = 6;
        rightNode.left = leftLeafOf6;
        rightNode.right = rightLeafOf6;

        Node rootNode = new Node();
        rootNode.data = 4;
        rootNode.left = leftNode;
        rootNode.right = rightNode;

        return rootNode;
    }
}