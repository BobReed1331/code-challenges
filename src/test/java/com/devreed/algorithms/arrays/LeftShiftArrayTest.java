package com.devreed.algorithms.arrays;

import com.google.common.collect.ImmutableList;

import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import static org.junit.Assert.*;

/**
 * See LeftShiftArray problem statement
 */
public class LeftShiftArrayTest {

    @Test
    public void testLeftShift(){
        List<Integer> result = LeftShiftArray.shiftLeft(new Integer[]{1,2,3,4,5}, 4);
        List<Integer> expectedResult = Arrays.asList(5,1,2,3,4);

        assertEquals(expectedResult, result);
    }

    @Test
    public void testMajorShift() throws FileNotFoundException {
        List<Integer> actualResult = LeftShiftArray.test();
        List<Integer> expectedResult = createToExpectedResult();

        assertEquals(actualResult.size(), expectedResult.size());
        assertEquals(expectedResult, actualResult);
    }

    private List<Integer> createToExpectedResult() throws FileNotFoundException {
        Scanner in = new Scanner(new File("src/main/resources/arrayShift/expectedResult.txt"));
        int n = in.nextInt();
        Integer[] array = LeftShiftArray.createArray(in, n);

        return  ImmutableList.copyOf(array);
    }
}