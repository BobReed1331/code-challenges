package com.devreed.algorithms.degreeSeparation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.devreed.algorithms.separation.Actor;
import com.devreed.algorithms.separation.DegreeOfSeparationCalculator;
import com.devreed.algorithms.separation.Movie;
import com.devreed.algorithms.separation.Role;

import static org.junit.Assert.*;

public class TestDegreesOfseparation {

    Actor connery, duffy, jolie, stallone, disconnected;
	
	@Before
	public void setUp() throws Exception {
        List<Role> conneryRoles = new ArrayList<>();
        connery = new Actor("Sean Connery", conneryRoles);

	    List<Role> jpRoles = new ArrayList<>();
	    Movie jurassicPark = new Movie("Jurassic Park", jpRoles);

	    Role jpLead = new Role(jurassicPark, connery,"Jurassic Park Lead");
	    jpRoles.add(jpLead);
        conneryRoles.add(jpLead);

        List<Role> duffyRoles = new ArrayList<>();
        duffy = new Actor("Patrick Duffy", duffyRoles);

        List<Role> gbRoles = new ArrayList<>();
        Movie ghostBusters = new Movie("Ghost Busters", gbRoles);

        Role jpSecondary = new Role(jurassicPark, duffy,"Jurassic Park Secondary");
        Role gbLead = new Role(ghostBusters, duffy,"GhostBusters Lead");

        jpRoles.add(jpSecondary);
        duffyRoles.add(jpSecondary);
        duffyRoles.add(gbLead);
        gbRoles.add(gbLead);

        List<Role> jolieRoles = new ArrayList<>();
        jolie = new Actor("Angelina Jolie", jolieRoles);
        Role gbSecondary = new Role(ghostBusters, jolie,"GhostBusters Secondary");
        gbRoles.add(gbSecondary);
        jolieRoles.add(gbSecondary);

        List<Role> stalloneRoles = new ArrayList<>();
        stallone = new Actor("Sylvester Stallone", stalloneRoles);

        List<Role> turtleRoles = new ArrayList<>();
        Movie ninjaTurtles = new Movie("Ninja Turtles", turtleRoles);

        Role turtlesLead = new Role(ninjaTurtles, stallone,"NinjaTurtles Lead");
        Role turtlesSecondary = new Role(ninjaTurtles, jolie,"NinjaTurtles Secondary");
        turtleRoles.add(turtlesLead);
        turtleRoles.add(turtlesSecondary);
        jolieRoles.add(turtlesSecondary);
        stalloneRoles.add(turtlesLead);

        disconnected = new Actor("disconnected", Collections.emptyList());
	}

	@Test
	public void test() {
		
		DegreeOfSeparationCalculator calculator = new DegreeOfSeparationCalculator();
		assertEquals(0, calculator.calculateDegreesOfSeparation(connery, connery));
        assertEquals(1, calculator.calculateDegreesOfSeparation(connery, duffy));
        assertEquals(2, calculator.calculateDegreesOfSeparation(connery, jolie));
        assertEquals(3, calculator.calculateDegreesOfSeparation(connery, stallone));

        assertEquals(1, calculator.calculateDegreesOfSeparation(jolie, duffy));
        assertEquals(1, calculator.calculateDegreesOfSeparation(jolie, stallone));
        assertEquals(2, calculator.calculateDegreesOfSeparation(stallone, duffy));

		/* Disconnected Actor */
        assertEquals(-1, calculator.calculateDegreesOfSeparation(connery, disconnected));
	}

}
